<?php 
include_once("funcoes/funcoes.php");

$templates = getTemplates();

if(isset($_POST['formpagina'])) {
	$item = array(
		"nome" => $_POST['nome'],
		"descricao" => $_POST['descricao'],
		"keywords" => $_POST['keywords'],
		"email" => $_POST['email'],
		"status" => $_POST['status'],
		"msg_manutencao" => $_POST['msg_manutencao'],
		"template_index" => $_POST['template_index'],
		"template_interna" => $_POST['template_interna'],
	);

	if(isset($_FILES['logo']['tmp_name'])) {
		$tmp = $_FILES['logo']['tmp_name'];
		$destino = "../uploads/" . $_FILES['logo']['name'];
		if(move_uploaded_file($tmp, $destino)) {
			$item['logo'] = str_replace("../", "", $destino);
		}
	}
	
	if(!isset($item['logo'])) {
		$config = lerArquivo('../assets/config/config.txt');
		$item['logo'] = $config['logo'];
	}

	if(file_put_contents("../assets/config/config.txt", serialize($item))) {
		header("Location: config.php?msg=1");
	}
	else {
		header("Location: config.php?msg=2");
	}
}
else {
	if(file_exists("../assets/config/config.txt")) {
		$conteudo = file_get_contents("../assets/config/config.txt");
		$item = unserialize($conteudo);
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Configurações</h1>
	
		<?php if(isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
			<div class="alert alert-sucesso">
				<p>Dados atualizados com sucesso!</p>
			</div>
		<?php } ?>

		<?php 
		if(!$erro) {
			include("inc/formulario-configuracoes.php");
		}
		else {
			echo '<div class="alert alert-erro">Nome de arquivo inválido</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>