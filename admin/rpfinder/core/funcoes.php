<?php

Class RPFinder {

	private $UploadPath = '';

	public function __construct($path = '') {
		$this->UploadPath = $path;
	}

	public function setUploadPath($path) {
		$this->UploadPath = $path;
	}

	public function getUploadPath() {
		return $this->UploadPath;
	}

	public function is_session() {
		if(isset($_SESSION['usuario'])) {
			return true;
		}
		return false;
	}

	public function readFolder() {
		$dir = opendir($this->UploadPath);
		$files = array();
		while (($file = readdir($dir)) !== false) {
			if(is_file($this->UploadPath . $file)) {
				$files[] = array(
					"name" => $file,
					"path" => $this->UploadPath . $file
				);
			}
		}
		closedir($dir);
		return $files;
	}

	public function upload($file = array()) {
		if($file) {
			$temp = $file['tmp_name'];
			$final = $this->UploadPath . $file['name'];
			if(move_uploaded_file($temp, $final)) {
				return true;
			}
		}
		return false;
	}

}