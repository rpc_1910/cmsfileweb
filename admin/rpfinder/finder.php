<?php

include_once("core/funcoes.php");

$Finder = new RPFinder('../../uploads/');

$arquivos = $Finder->readFolder();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>RPFinder</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<header>
		<form action="upload.php?target=<?php if(isset($_GET['target'])) { echo $_GET['target']; }  ?>" method="POST" class="formulario-upload" enctype="multipart/form-data">
		
			<div class="form-group">
				<label for="imagem">Escolha a imagem para upload</label>
				<input type="file" name="imagem" id="imagem" placeholder="Selecione a imagem para upload" accept="image/*" required>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</form>
	</header>
	<div class="container">
		<?php if($arquivos) { ?>
			<?php foreach ($arquivos as $arquivo) { ?>
			<div class="item-foto" data-arquivo="<?php echo $arquivo['name'] ?>">
				<div class="image"><p><img src="<?php echo $arquivo['path'] ?>" class="img-responsive"></p></div>
				<p><?php echo $arquivo['name'] ?></p>
			</div>
			<?php } ?>
		<?php } else { ?>
			<p class="text-center">Nenhuma arquivo encontrado</p>
		<?php } ?>
	</div>
	<input type="hidden" id="target" value="<?php if(isset($_GET['target'])) { echo $_GET['target']; }  ?>">
	<script src="js/jquery.min.js"></script>
	<script src="js/lib.js"></script>
</body>
</html>