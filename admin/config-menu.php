<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	$links = array();
	foreach ($_POST['menu'] as $item) {
		if(!empty($item['nome'])) {
			$links[] = array(
				"nome" => $item['nome'],
				"href" => $item['link'],
			);
		}
	}
	file_put_contents("../assets/config/menu.txt", serialize($links));
	header("Location: config-menu.php?msg=1");
}
else {
	$links = array();
	if(file_exists("../assets/config/menu.txt")) {
		$links = unserialize(file_get_contents("../assets/config/menu.txt"));
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Menu Principal</h1>
		<div class="alert alert-info">
			<p>Para excluir um link deixe os campos vazios</p>
		</div>
		<?php if(isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
			<div class="alert alert-sucesso">
				<p>Menu atualizado com sucesso!</p>
			</div>
		<?php } ?>
		<form action="" method="POST" class="formulario">
			<?php if($links) { ?>
				<?php $contador = 0; 
				foreach ($links as $link) { ?>
					<fieldset>
						<legend>Link</legend>
						<div class="item-form">
							<label>Nome do link: </label>
							<input type="text" name="menu[<?php echo $contador; ?>][nome]" placeholder="Digite o nome do link" value="<?php echo $link['nome'] ?>" >
						</div>

						<div class="item-form">
							<label>Endereço do link: </label>
							<input type="text" name="menu[<?php echo $contador; ?>][link]" placeholder="Digite o endereço do link" value="<?php echo $link['href'] ?>" >
						</div>
					</fieldset>
					<?php $contador++; ?>
				<?php } ?>
			<?php } else { ?>
			<fieldset>
				<legend>Link</legend>
				<div class="item-form">
					<label>Nome do link: </label>
					<input type="text" name="menu[0][nome]" placeholder="Digite o nome do link" >
				</div>

				<div class="item-form">
					<label>Endereço do link: </label>
					<input type="text" name="menu[0][link]" placeholder="Digite o endereço do link" >
				</div>
			</fieldset>
			<?php } ?>

			<fieldset id="botoes">
				<input type="hidden" name="formpagina" value="1">
				<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
				<button type="button" class="btn btn-editar btn-big" onclick="add()">Adicionar Link</button>
			</fieldset>
		</form>
	</div>
</div>

<style>
	fieldset {
		margin-bottom:20px;
	}
</style>

<script>
	<?php if($links) { ?>
	var numero_link = <?php echo $contador ?>;
	<?php } else { ?>
	var numero_link = 1;
	<?php }  ?>
	function add() {
		html = "<fieldset>";
		html += "<legend>Link</legend>";
		html += '<div class="item-form">';
		html += '<label>Nome do link: </label>';
		html += '<input type="text" name="menu[' + numero_link + '][nome]" placeholder="Digite o nome do link" >';
		html += '</div>';
		html += '<div class="item-form">';
		html += '<label>Endereço do link: </label>';
		html += '<input type="text" name="menu[' + numero_link + '][link]" placeholder="Digite o endereço do link" >';
		html += '</div>';
		html += '</fieldset>';
		$("#botoes").before(html);
		numero_link++;
	}
</script>

<?php include("footer.php"); ?>