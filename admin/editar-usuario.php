<?php 
include_once("funcoes/funcoes.php");
include_once("config/users.php");
if(isset($_POST['formpagina'])) {
	$referencia = getUser( $_GET['user'] );
	
	if($referencia) {
		unset($user[ $_GET['user'] ]);
	}

	$novo_usuario = array(
		"user" => $_POST['user'],
		"nome" => $_POST['nome'],
		"nivel" => $_POST['nivel'],
	);

	if(isset($_POST['pass'])) {
		$novo_usuario['pass'] = md5($_POST['pass']);
	}

	
	$usuario_ok = array_merge($referencia, $novo_usuario);
	$user[ $_POST['user'] ] = $usuario_ok;

	$doc = "<?php \n\n";
	$doc .= '$user = ' . var_export($user, true) . ';';

	if(file_put_contents("config/users.php", $doc)) {
		header("Location: todos-usuarios.php?msg=1");
	}
	else {
		header("Location: todos-usuarios.php?msg=2");
	}
}
else {
	if(isset($_GET['user']) && $item = getUser( $_GET['user'] )) {
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Editar usuário</h1>
		<?php 
		if(!$erro) {
			include("inc/formulario-usuarios.php");
		}
		else {
			echo '<div class="alert alert-erro">Usuário não encontrado</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>