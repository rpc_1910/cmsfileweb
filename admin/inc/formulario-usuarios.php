<form action="" method="POST" class="formulario">
<fieldset>
	<legend>Preencha os dados corretamente</legend>
	<div class="item-form">
		<label for="nome">Nome: </label>
		<input type="text" name="nome" id="nome" placeholder="Digite o nome do usuário" value="<?php echo $item['nome']; ?>" required autofocus>
	</div>

	<div class="item-form">
		<label for="user">Usuário de acesso: </label>
		<input type="text" name="user" id="user" placeholder="Digite o nome de usuário" value="<?php echo $item['user'] ?>" pattern="[a-z0-9_]+" required>
		<?php if(isset($item['erro'])) { ?>
		<div class="alert alert-erro">O usuário informado já esta cadastrado no sistema!</div>
		<?php } ?>
	</div>

	<div class="item-form">
		<?php if(isset($_GET['user'])) { ?>
			<a href="javascript:void(0)" id="alterar-senha" class="btn btn-visualizar" onclick="alterSenha();">Clique aqui para alterar a senha</a>
		<?php } else { ?>
			<label for="pass">Senha: </label>
			<input type="password" name="pass" id="pass" placeholder="Digite a senha" value="<?php echo $item['pass']; ?>" required>
		<?php } ?>
	</div>

	<div class="item-form">
		<label for="nivel">Nível de acesso: </label>
		<select name="nivel" id="nivel" required>
			<option value="">Selecione</option>
			<option value="1" <?php if($item['nivel'] == "1") { echo 'selected="selected"'; } ?>>Administrador</option>
			<option value="2" <?php if($item['nivel'] == "2") { echo 'selected="selected"'; } ?>>Desenvolvedor</option>
			<option value="3" <?php if($item['nivel'] == "3") { echo 'selected="selected"'; } ?>>Editor</option>
		</select>
	</div>

	<input type="hidden" name="formpagina" value="1">
	<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
</fieldset>
</form>

<script>
	function alterSenha() {
		html = '<label for="pass">Senha: </label>';
		html += '<input type="password" name="pass" id="pass" placeholder="Digite a senha" required>';
		$("#alterar-senha").before(html).remove();
	}
</script>