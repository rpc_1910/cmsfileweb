<form action="" method="POST" class="formulario">
	<fieldset>
		<legend>Preencha os dados corretamente</legend>
		<div class="item-form">
			<label for="arquivo">Nome do arquivo: <small>(Opicional)</small></label>
			<input type="text" name="arquivo" id="arquivo" placeholder="Digite o nome do arquivo" value="<?php echo $item['arquivo']; ?>">
		</div>
		<div class="item-form">
			<label for="nome">Título da página: </label>
			<input type="text" name="nome" id="nome" placeholder="Digite o nome da página" value="<?php echo $item['nome']; ?>" required autofocus>
		</div>

		<div class="item-form">
			<label for="descricao">Meta Descrição: </label>
			<textarea name="descricao" id="descricao" placeholder="Preencha a Meta Descrição da página"><?php echo $item['descricao']; ?></textarea>
		</div>

		<div class="item-form">
			<label for="keywords">Meta Keywords: </label>
			<textarea name="keywords" id="keywords" placeholder="Preencha as Meta Keywords da página"><?php echo $item['keywords']; ?></textarea>
		</div>

		<div class="item-form">
			<label for="conteudo">Conteúdo: </label>
			<button type="button" class="btn btn-inserir" onclick="RPFinder('conteudo')">Inserir imagem</button> <br><br>
			<textarea name="conteudo" id="conteudo" class="editor" placeholder="Preencha o conteúdo da página"><?php echo $item['conteudo']; ?></textarea>
		</div>

		<input type="hidden" name="formpagina" value="1">
		<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
	</fieldset>
</form>