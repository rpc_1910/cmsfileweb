<form action="" method="POST" class="formulario formulario-cores" enctype="multipart/form-data">
	<fieldset>
		<legend>Configurações gerais</legend>
		
		<div class="item-form">
			<label for="bgcorpo">Background do site: </label>
			<input type="color" name="bgcorpo" id="bgcorpo" placeholder="Background do site" value="<?php echo $item['bgcorpo']; ?>" required>
		</div>

		<div class="item-form">
			<label for="bgtopo">Background do topo: </label>
			<input type="color" name="bgtopo" id="bgtopo" placeholder="Background do topo" value="<?php echo $item['bgtopo']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_logo">Cor da logo: </label>
			<input type="color" name="color_logo" id="color_logo" value="<?php echo $item['color_logo']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_texto">Cor do texto: </label>
			<input type="color" name="color_texto" id="color_texto" value="<?php echo $item['color_texto']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_titulo">Cor dos títulos: </label>
			<input type="color" name="color_titulo" id="color_titulo" value="<?php echo $item['color_titulo']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_link">Cor do link: </label>
			<input type="color" name="color_link" id="color_link" value="<?php echo $item['color_link']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_link_ativo">Cor do link ativo: </label>
			<input type="color" name="color_link_ativo" id="color_link_ativo" value="<?php echo $item['color_link_ativo']; ?>" required>
		</div>
	</fieldset>

	<fieldset>
		<legend>Menu</legend>

		<div class="item-form">
			<label for="bgmenu">Background: </label>
			<input type="color" name="bgmenu" id="bgmenu" value="<?php echo $item['bgmenu']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_menu">Cor dos links: </label>
			<input type="color" name="color_menu" id="color_menu" value="<?php echo $item['color_menu']; ?>" required>
		</div>

		<div class="item-form">
			<label for="color_menu_hover">Cor dos links ativos: </label>
			<input type="color" name="color_menu_hover" id="color_menu_hover" value="<?php echo $item['color_menu_hover']; ?>" required>
		</div>

		<div class="item-form">
			<label for="bg_menu_hover">Background do link: </label>
			<input type="color" name="bg_menu_hover" id="bg_menu_hover" value="<?php echo $item['bg_menu_hover']; ?>" required>
		</div>
	</fieldset>

	<fieldset>
		<legend>Rodapé</legend>
		<div class="item-form">
			<label for="bgrodape">Background: </label>
			<input type="color" name="bgrodape" id="bgrodape" value="<?php echo $item['bgrodape']; ?>" required>
		</div>
		<div class="item-form">
			<label for="color_rodape">Cor do texto: </label>
			<input type="color" name="color_rodape" id="color_rodape" value="<?php echo $item['color_rodape']; ?>" required>
		</div>
	</fieldset>

	<fieldset>
		<input type="hidden" name="formpagina" value="1">
		<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
	</fieldset>
</form>

<style>
	fieldset {
		margin-bottom:20px;
	}
	<?php if($item) { ?>
		<?php foreach ($item as $key => $value) { ?>
			.formulario-cores input[name="<?php echo $key; ?>"] { background-color: <?php echo $value; ?> }
		<?php } ?>
	<?php } ?>
</style>