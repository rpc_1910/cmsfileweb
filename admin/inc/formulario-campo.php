<form action="" method="POST" class="formulario">
	<fieldset>
		<legend>Preencha os dados corretamente</legend>
		<div class="item-form">
			<label for="arquivo">Nome do arquivo: <small>(Opicional)</small></label>
			<input type="text" name="arquivo" id="arquivo" placeholder="Digite o nome do arquivo" value="<?php echo $item['arquivo']; ?>">
		</div>
		<div class="item-form">
			<label for="nome">Título do campo: </label>
			<input type="text" name="nome" id="nome" placeholder="Digite o nome do campo" value="<?php echo $item['nome']; ?>" required autofocus>
		</div>
		<div class="item-form">
			<label for="classe">Classe do campo: </label>
			<input type="text" name="classe" id="classe" placeholder="Digite a classe do campo" value="<?php echo $item['classe']; ?>" >
		</div>
		<div class="item-form">
			<label for="situacao">Situação do Campo</label>
			<select name="situacao" id="situacao" required>
				<option value="">Selecione</option>
				<option value="1" <?php echo ($item['situacao'] === "1") ? 'selected="selected"': NULL; ?>>Ativo</option>
				<option value="0" <?php echo ($item['situacao'] === "0") ? 'selected="selected"': NULL; ?>>Inativo</option>
			</select>
		</div>
		<div class="item-form">
			<label for="conteudo">Conteúdo: </label>
			<button type="button" class="btn btn-inserir" onclick="RPFinder('conteudo')">Inserir imagem</button> <br><br>
			<textarea name="conteudo" id="conteudo" class="editor" placeholder="Preencha o conteúdo da página"><?php echo $item['conteudo']; ?></textarea>
		</div>

		<input type="hidden" name="formpagina" value="1">
		<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
	</fieldset>
</form>