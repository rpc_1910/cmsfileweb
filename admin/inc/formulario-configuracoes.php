<form action="" method="POST" class="formulario" enctype="multipart/form-data">
	<fieldset>
		<legend>Configurações do site</legend>
		
		<div class="item-form">
			<label for="logo">Logo: </label>
			<input type="file" name="logo" id="logo" placeholder="Selecione a logo do site" accept="image/*">
		</div>

		<div class="item-form">
			<label for="nome">Título do site: </label>
			<input type="text" name="nome" id="nome" placeholder="Digite o título do site" value="<?php echo $item['nome']; ?>" required autofocus>
		</div>

		<div class="item-form">
			<label for="descricao">Meta Descrição: </label>
			<textarea name="descricao" id="descricao" placeholder="Preencha a Meta Descrição da página"><?php echo $item['descricao']; ?></textarea>
		</div>

		<div class="item-form">
			<label for="keywords">Meta Keywords: </label>
			<textarea name="keywords" id="keywords" placeholder="Preencha as Meta Keywords da página"><?php echo $item['keywords']; ?></textarea>
		</div>

		<div class="item-form">
			<label for="email">E-mail de contato:</label>
			<input type="email" name="email" id="email" value="<?php echo $item['email'] ?>" required >
		</div>
	</fieldset>

	<fieldset>
		<legend>Status de publicação</legend>
		<div class="item-form">
			<label for="status">Status:</label>
			<select name="status" id="status" required>
				<option value="">Selecione</option>
				<option value="1" <?php if($item['status'] == "1") { echo 'selected="selected"'; } ?>>Publicado</option>
				<option value="2" <?php if($item['status'] == "2") { echo 'selected="selected"'; } ?>>Em manutenção</option>
			</select>
		</div>		

		<div class="item-form">
			<label for="msg_manutencao">Mensagem de manutenção: </label>
			<button type="button" class="btn btn-inserir" onclick="RPFinder('msg_manutencao')">Inserir imagem</button> <br><br>
			<textarea name="msg_manutencao" id="msg_manutencao" class="editor" placeholder="Preencha o conteúdo que será exibido quando o site estiver me manutenção"><?php echo $item['msg_manutencao']; ?></textarea>
		</div>
	</fieldset>

	<fieldset>
		<legend>Templates</legend>
		<div class="item-form">
			<label for="template_index">Template Página inicial:</label>
			<select name="template_index" id="template_index" required>
				<option value="">Selecione</option>
				<?php if(isset($templates['index'])) { ?>
					<?php foreach ($templates['index'] as $page) { ?>
					<option value="<?php echo $page; ?>" <?php if( $page == $item['template_index'] ) { echo 'selected="selected"'; } ?>><?php echo $page; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>

		<div class="item-form">
			<label for="template_interna">Template Página interna:</label>
			<select name="template_interna" id="template_interna" required>
				<option value="">Selecione</option>
				<?php if(isset($templates['interna'])) { ?>
					<?php foreach ($templates['interna'] as $page) { ?>
					<option value="<?php echo $page; ?>" <?php if( $page == $item['template_interna'] ) { echo 'selected="selected"'; } ?>><?php echo $page; ?></option>
					<?php } ?>
				<?php } ?>
			</select>
		</div>
	</fieldset>

	<fieldset>
		<input type="hidden" name="formpagina" value="1">
		<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
	</fieldset>
</form>

<style>
	fieldset {
		margin-bottom:20px;
	}
</style>