<form action="" method="POST" class="formulario">
	<fieldset>
		<legend>Preencha os dados corretamente</legend>
		<div class="item-form">
			<label for="arquivo">Nome do arquivo: </label>
			<input type="text" name="arquivo" id="arquivo" placeholder="Digite o nome do arquivo" value="<?php echo $item['arquivo']; ?>" pattern="[a-zA-Z0-9-_.]+" autofocus required>
		</div>

		<div class="item-form">
			<label for="conteudo">Conteúdo: </label>
			<textarea name="conteudo" id="conteudo" class="editor" placeholder="Preencha o conteúdo do template"><?php echo $item['conteudo']; ?></textarea>
		</div>

		<input type="hidden" name="formpagina" value="1">
		<button type="submit" class="btn btn-enviar btn-big">Salvar</button>
	</fieldset>
</form>