<?php 
include_once("funcoes/funcoes.php");
session_start();
if(basename($_SERVER['PHP_SELF']) == "index.php") {
	if(isset($_SESSION['usuario'])) {
		header("Location: painel.php");
	}
}
else {
	if(!isset($_SESSION['usuario'])) {
		header("Location: index.php?error=2");
	}
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>CMS File Web 1.0</title>
	<link rel="stylesheet" href="../css/grid.css">
	<link rel="stylesheet" href="css/estilos.css">
	<script src="../js/jquery.min.js"></script>
</head>
<body>
	<header>
		<!-- <div class="container"> -->
			<div class="logo col-5">
				<a href="index.php">CMS File Web 1.0</a>
			</div>
			<div class="col-5 info-logado">
				<?php if(isset($_SESSION['usuario'])) { ?>
					Olá <?php echo $_SESSION['usuario']['nome']; ?>
				<?php } ?>
				<a href="../" target="_blank">Visualizar site</a>
			</div>
			<div class="clearfix"></div>
		<!-- </div> -->
	</header>