<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	$item = array(
		"nome" => htmlentities(strip_tags($_POST['nome'])),
		"classe" => $_POST['classe'],
		"situacao" => $_POST['situacao'],
		"conteudo" => $_POST['conteudo']
	);
	$nome_arquivo = ($_POST['arquivo'] != "") ? nome_arquivo($_POST['arquivo']) : nome_arquivo($_POST['nome']);
	if(file_put_contents("../assets/campos/" . $nome_arquivo, serialize($item))) {
		header("Location: todos-campos.php?msg=1");
	}
	else {
		header("Location: todos-campos.php?msg=2");
	}
}
else {
	$item = array(
		"arquivo" => "",
		"nome" => "",
		"situacao" => "",
		"classe" => "",
		"conteudo" => ""
	);
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Inserir campos</h1>
		<?php include("inc/formulario-campo.php"); ?>
	</div>
</div>


<?php include("footer.php"); ?>