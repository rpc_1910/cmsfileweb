<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['usuario']) && isset($_POST['senha'])) {
	$usuario = htmlentities($_POST['usuario']);
	$senha = md5( htmlentities($_POST['senha']) );
	include_once("config/users.php");
	if(validUser( $usuario, $senha )) {
		session_start();
		$_SESSION['usuario'] = getUser($usuario);
		header("Location: painel.php");
	}
	else {
		header("location: index.php?error=1");
	}
}
include("header.php"); ?>

<!-- Formulário de login -->
<form action="" method="POST" class="formulario-login">
	<fieldset>
		<legend>Bem vindo ao CMS File Web 1.0</legend>
		<div class="item">
			<input type="text" name="usuario" placeholder="Usuário" required>
		</div>
		<div class="item">
			<input type="password" name="senha" placeholder="Senha" required>
		</div>
		<button class="btn-login">Entrar</button>
	</fieldset>
</form>


<?php include("footer.php"); ?>