<ul>
	<li><a href="painel.php">Painel</a></li>
	<li><a href="todos-campos.php">Campos</a></li>
	<li><a href="todas-paginas.php">Páginas</a></li>
	<li><a href="config-menu.php">Menu Principal</a></li>

	<?php if($_SESSION['usuario']['nivel'] == "1" || $_SESSION['usuario']['nivel'] == "2") { ?>
	<li><a href="todos-estilos.php">Editor CSS</a></li>
	<li><a href="config-design.php">Design</a></li>
	<li><a href="todos-templates.php">Templates</a></li>
	<?php } ?>

	<?php if($_SESSION['usuario']['nivel'] == "1") { ?>
	<li><a href="todos-usuarios.php">Usuários</a></li>
	<li><a href="config.php">Configurações</a></li>
	<?php } ?>
	<li><a href="logout.php">Sair</a></li>
</ul>