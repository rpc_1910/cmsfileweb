<?php include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Todos os Usuários</h1>
		<div class="opcoes-modulo">
			<a href="inserir-usuario.php" class="btn btn-inserir">Novo Usuário</a>
		</div>
		<div class="mensagens">
			<?php if(isset($_GET['msg'])) { 
				if($_GET['msg'] == 1) {
					echo '<div class="alert alert-sucesso">Usuário criado com sucesso</div>';
				}
				else if($_GET['msg'] == 2) {
					echo '<div class="alert alert-erro">Ops... Ocorreu um erro ao salvar o arquivo</div>';
				}
			} ?>
		</div>
		<table class="tabela">
			<tbody>
				<tr>
					<th>Nome</th>
					<th>Usuário</th>
					<th>Nível de acesso</th>
					<th>Opções</th>
				</tr>
				<?php 
				include_once("config/users.php");
				if(!$user) { ?>
				<tr>
					<td class="nenhum-resultado text-center" colspan="4">Nenhuma página foi encontrada</td>
				</tr>
				<?php } else { 
					foreach ($user as $item) { ?>
				<tr>
					<td><?php echo $item['nome'] ?></td>
					<td><?php echo $item['user'] ?></td>
					<td class="text-center"><?php echo getNivelAcesso($item['nivel']); ?></td>
					<td>
						<a href="editar-usuario.php?user=<?php echo $item['user'] ?>" class="btn btn-editar">Editar</a>
						<a href="excluir-usuario.php?user=<?php echo $item['user'] ?>" class="btn btn-excluir" onclick="return excluir();">Excluir</a>
					</td>
				</tr>
				<?php }} ?>
			</tbody>
		</table>
	</div>
</div>


<?php include("footer.php"); ?>