<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	if($_GET['arquivo'] != $_POST['arquivo']) {
		@unlink("../assets/templates/" . (string)$_GET['arquivo']);
	}

	$nome_arquivo = (strstr($_POST['arquivo'], ".html")) ? $_POST['arquivo'] : $_POST['arquivo'] . ".html";
	if(file_put_contents("../assets/templates/" . $nome_arquivo, (string)$_POST['conteudo'])) {
		header("Location: todos-templates.php?msg=1");
	}
	else {
		header("Location: todos-templates.php?msg=2");
	}
}
else {
	if(isset($_GET['arquivo']) && file_exists("../assets/templates/" . (string)$_GET['arquivo'])) {
		$item = array(
			"arquivo" => $_GET['arquivo'],
			"conteudo" => file_get_contents("../assets/templates/" . $_GET['arquivo'])
		);
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Editar Template</h1>
		<?php 
		if(!$erro) {
			include("inc/formulario-templates.php");
		}
		else {
			echo '<div class="alert alert-erro">Nome de arquivo inválido</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>