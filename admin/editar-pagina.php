<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	if($_GET['arquivo'] != $_POST['arquivo']) {
		@unlink("../assets/paginas/" . (string)$_GET['arquivo']);
	}
	$item = array(
		"nome" => htmlentities(strip_tags($_POST['nome'])),
		"descricao" => htmlentities(strip_tags($_POST['descricao'])),
		"keywords" => htmlentities(strip_tags($_POST['keywords'])),
		"conteudo" => $_POST['conteudo']
	);
	$nome_arquivo = ($_POST['arquivo'] != "") ? nome_arquivo($_POST['arquivo']) : nome_arquivo($_POST['nome']) ;
	if(file_put_contents("../assets/paginas/" . $nome_arquivo, serialize($item))) {
		header("Location: todas-paginas.php?msg=1");
	}
	else {
		header("Location: todas-paginas.php?msg=2");
	}
}
else {
	if(isset($_GET['arquivo']) && file_exists("../assets/paginas/" . (string)$_GET['arquivo'])) {
		$conteudo = file_get_contents("../assets/paginas/" . $_GET['arquivo']);
		$item = unserialize($conteudo);
		$item['arquivo'] = $_GET['arquivo'];
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Editar páginas</h1>
		<?php 
		if(!$erro) {
			include("inc/formulario-paginas.php");
		}
		else {
			echo '<div class="alert alert-erro">Nome de arquivo inválido</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>