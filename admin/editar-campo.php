<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	if($_GET['arquivo'] != $_POST['arquivo']) {
		@unlink("../assets/campos/" . (string)$_GET['arquivo']);
	}
	$item = array(
		"nome" => htmlentities(strip_tags($_POST['nome'])),
		"classe" => $_POST['classe'],
		"situacao" => $_POST['situacao'],
		"conteudo" => $_POST['conteudo'],
	);
	$nome_arquivo = ($_POST['arquivo'] != "") ? nome_arquivo($_POST['arquivo']) : nome_arquivo($_POST['nome']) ;
	if(file_put_contents("../assets/campos/" . $nome_arquivo, serialize($item))) {
		header("Location: todos-campos.php?msg=1");
	}
	else {
		header("Location: todos-campos.php?msg=2");
	}
}
else {
	if(isset($_GET['arquivo']) && file_exists("../assets/campos/" . (string)$_GET['arquivo'])) {
		$conteudo = file_get_contents("../assets/campos/" . $_GET['arquivo']);
		$item = unserialize($conteudo);
		$item['arquivo'] = $_GET['arquivo'];
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Editar Campos</h1>
		<?php 
		if(!$erro) {
			include("inc/formulario-campo.php");
		}
		else {
			echo '<div class="alert alert-erro">Nome de arquivo inválido</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>