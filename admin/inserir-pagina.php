<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	$item = array(
		"nome" => htmlentities(strip_tags($_POST['nome'])),
		"descricao" => htmlentities(strip_tags($_POST['descricao'])),
		"keywords" => htmlentities(strip_tags($_POST['keywords'])),
		"conteudo" => $_POST['conteudo']
	);
	$nome_arquivo = ($_POST['arquivo'] != "") ? nome_arquivo($_POST['arquivo']) : nome_arquivo($_POST['nome']);
	if(file_put_contents("../assets/paginas/" . $nome_arquivo, serialize($item))) {
		header("Location: todas-paginas.php?msg=1");
	}
	else {
		header("Location: todas-paginas.php?msg=2");
	}
}
else {
	$item = array(
		"arquivo" => "",
		"nome" => "",
		"descricao" => "",
		"keywords" => "",
		"conteudo" => ""
	);
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Inserir páginas</h1>
		<?php include("inc/formulario-paginas.php"); ?>
	</div>
</div>


<?php include("footer.php"); ?>