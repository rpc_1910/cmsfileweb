<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	$nome_arquivo = (strstr($_POST['arquivo'], ".html")) ? $_POST['arquivo'] : $_POST['arquivo'] . ".html";
	if(file_put_contents("../assets/templates/" . $nome_arquivo, (string)$_POST['conteudo'])) {
		header("Location: todos-templates.php?msg=1");
	}
	else {
		header("Location: todos-templates.php?msg=2");
	}
}
else {
	$item = array(
		"arquivo" => "",
		"conteudo" => ""
	);
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Criar Template</h1>
		<?php include("inc/formulario-templates.php"); ?>
	</div>
</div>


<?php include("footer.php"); ?>