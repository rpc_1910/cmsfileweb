<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	$nome_arquivo = (strstr($_POST['arquivo'], ".css")) ? $_POST['arquivo'] : $_POST['arquivo'] . ".css";
	if(file_put_contents("../css/" . $nome_arquivo, (string)$_POST['conteudo'])) {
		header("Location: todos-estilos.php?msg=1");
	}
	else {
		header("Location: todos-estilos.php?msg=2");
	}
}
else {
	$item = array(
		"arquivo" => "",
		"conteudo" => ""
	);
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Criar arquivo CSS</h1>
		<?php include("inc/formulario-estilos.php"); ?>
	</div>
</div>


<?php include("footer.php"); ?>