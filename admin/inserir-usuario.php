<?php 
include_once("funcoes/funcoes.php");
if(isset($_POST['formpagina'])) {
	include_once("config/users.php");
	
	// Verifica se o nome de usuário inserido já existe
	if(!isset($user[ (string)$_POST['user'] ])) {
		$user[ (string)$_POST['user'] ] = array(
			"user" => $_POST['user'],
			"pass" => md5($_POST['pass']),
			"nome" => $_POST['nome'],
			"nivel" => $_POST['nivel'],
		);

		$doc = "<?php \n\n";
		$doc .= '$user = ' . var_export($user, true) . ';';

		if(file_put_contents("config/users.php", $doc)) {
			header("Location: todos-usuarios.php?msg=1");
		}
		else {
			header("Location: todos-usuarios.php?msg=2");
		}
	}
	else {
		$item = array(
			"user" => (string)$_POST['user'],
			"pass" => (string)$_POST['pass'],
			"nome" => (string)$_POST['nome'],
			"nivel" => (string)$_POST['nivel'],
			"erro" => true
		);
	}
}
else {
	$item = array(
		"user" => "",
		"pass" => "",
		"nome" => "",
		"nivel" => "",
	);
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Inserir usuário</h1>
		<?php include("inc/formulario-usuarios.php"); ?>
	</div>
</div>


<?php include("footer.php"); ?>