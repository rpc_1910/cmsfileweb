<?php 
include_once("funcoes/funcoes.php");

if(isset($_POST['formpagina'])) {
	
	unset($_POST['formpagina']);

	$item = $_POST;

	gerarCss($item);
	
	if(file_put_contents("../assets/config/design.txt", serialize($item))) {
		header("Location: config-design.php?msg=1");
	}
	else {
		header("Location: config-design.php?msg=2");
	}
}
else {
	if(file_exists("../assets/config/design.txt")) {
		$conteudo = file_get_contents("../assets/config/design.txt");
		$item = unserialize($conteudo);
		//var_dump($item);
		$erro = false;
	}
	else {
		$erro = true;
	}
}
include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Design</h1>
	
		<?php if(isset($_GET['msg']) && $_GET['msg'] == 1) { ?>
			<div class="alert alert-sucesso">
				<p>Design atualizado com sucesso!</p>
			</div>
		<?php } ?>

		<?php 
		if(!$erro) {
			include("inc/formulario-design.php");
		}
		else {
			echo '<div class="alert alert-erro">Nome de arquivo inválido</div>';
		}
		?>
	</div>
</div>


<?php include("footer.php"); ?>