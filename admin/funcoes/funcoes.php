<?php

// Retorna um Array contendo todos os arquivos de determinado diretório
function lerDiretorio($caminho, $serialize = false) {
	$arquivos = array();
	if(is_dir($caminho)) {
		if($diretorio = opendir($caminho)) {
			while (($item = readdir($diretorio)) !== false) {
				$caminho_arquivo = str_replace("//", "/", ($caminho  . '/' . $item));
				if(is_file($caminho_arquivo)) {
					$arquivos[] = array(
						"nome" => $item,
						"caminho" => $caminho_arquivo,
						"data" => date("d/m/Y H:i:s", filemtime($caminho_arquivo)),
						"conteudo" => ($serialize) ? unserialize(file_get_contents($caminho_arquivo)) : array()
					);
				}
			}
		}
	}
	return $arquivos;
}

// Gera o nome do arquivo Otimizado para SEO sem espaços e com todos os caracteres minúsculos
function nome_arquivo($nome) {
	$nome = str_replace(".txt", "", $nome);
	$nome = str_replace(" ", "-", strtolower(trim($nome)));
	if($nome) {
		$nome .= ".txt";
	}
	else {
		$nome = time() . ".txt";
	}
	return $nome;
}

// Valida se o usuário e senha são válidos
function validUser($usuario, $senha) {
	global $user;
	if(isset($user[ $usuario ])) {
		if($user[$usuario]['user'] == $usuario && $user[$usuario]['pass'] == $senha) {
			return true;
		}
	}
	return false;
}

// Retorna os dados de determinado usuário através do seu nickname
function getUser($usuario) {
	global $user;
	if(isset($user[ $usuario ])) {
		return $user[ $usuario ];
	}
	return array();
}

// Deleta um arquivo do servidor
function deletar($caminho) {
	if(file_exists($caminho)) {
		@unlink($caminho);
	}
}

// Faz leitura de um arquivo TXT e retorna um Array com seu conteúdo
function lerArquivo($caminho) {
	if(file_exists($caminho)) {
		return unserialize(file_get_contents($caminho));
	}
	return array();
}

function getNivelAcesso($nivel) {
	switch ($nivel) {
		// Administrador
		case '1': return "Administrador";
		case '2': return "Desenvolvedor";
		case '3': return "Editor";
		default: return "Não encontrado";
	}
}

function getTemplates() {
	$arquivos = lerDiretorio('../assets/templates/');
	$templates = array();
	if($arquivos) {
		foreach ($arquivos as $item) {
			if(strstr($item['nome'], "index_")) {
				$templates['index'][] = $item['nome'];
			}
			else {
				$templates['interna'][] = $item['nome'];
			}
		}
	}
	return $templates;
}

function getCampo($caminho) {
	$campo = lerArquivo( $caminho );
	if($campo) {
		if($campo['situacao']) {
			$saida = '<div class="'. $campo['classe'] .'">'. $campo['conteudo'] .'</div>';
			return $saida;
		}
	}
	return "";
}

function getMenuPrincipal($caminho) {
	$menu = lerArquivo( $caminho );
	if($menu) {
		$saida = '<nav class="menu">
		<ul>';
		foreach ($menu as $link) {
			$saida .= '<li><a href="'. $link['href'] .'" title="'. $link['nome'] .'">'. $link['nome'] .'</a></li>';
		}
		$saida .= '</ul>
		</nav>';
		return $saida;
	}
	return "";
}

function getLogo($config) {
	if(is_file($_SERVER['DOCUMENT_ROOT'] . '/' . $config['logo'])) {
		return '<div class="logo"><a href="/index.php"><img src="/'. $config['logo'] .'" alt="'. $config['nome'] .'"></a></div>';
	}
	else {
		return '<div class="logo"><a href="/index.php">'. $config['nome'] .'</a></div>';
	}
}

function gerarCss($dados) {
	$css = '
	body { background-color: '. $dados['bgcorpo'] .'; color: '. $dados['color_texto'] .';}
	header { background-color: '. $dados['bgtopo'] .';}
	.logo a { color: '. $dados['color_logo'] .';}
	h1,h2,h3,h4,h5,h6 { color: '. $dados['color_titulo'] .';}
	a { color: '. $dados['color_link'] .';}
	a:hover { color: '. $dados['color_link_ativo'] .';}
	#menu-principal { background-color: '. $dados['bgmenu'] .';}
	#menu-principal a { color: '. $dados['color_menu'] .';}
	#menu-principal a:hover { color: '. $dados['color_menu_hover'] .'; background-color: '. $dados['bg_menu_hover'] .';}
	footer { background-color: '. $dados['bgrodape'] .'; color: '. $dados['color_rodape'] .'; }
	';
	file_put_contents("../assets/css/estilos.css", $css);
}

function getCss($caminho = '') {
	return '<link rel="stylesheet" href="/'. $caminho .'">';
}

function getConteudo($pagina) {
	$saida = '';
	if($pagina) {
		$saida .= '<h1>'. $pagina['nome'] .'</h1>';
		$saida .= nl2br($pagina['conteudo']);
	}
	else {
		$saida .= '<h1>Conteúdo não encontrado!</h1>
		<p>Aparentemente o conteúdo que você estava procurando não esta mais disponível ou foi removido!</p>';
	}
	return $saida;
}