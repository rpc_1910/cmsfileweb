function excluir() {
	return confirm("Deseja realmente excluir este item?");
}

function RPFinder(target) {
	window.open("rpfinder/finder.php?target=" + target, "RPFinder", "height=600,width=800,scrollbars=1,status=0,menubar=0,toolbar=0,titlebar=1,location=0");
}

function insertImagem(destino, caminho) {
	document.getElementsByName(destino)[0].value += '<img src="/uploads/' + caminho + '" alt="" />' + "\n";
}

$(function(){
	$("input[type='color']").bind("input", function(){
		$(this).css('background-color', $(this).val());
	});
});