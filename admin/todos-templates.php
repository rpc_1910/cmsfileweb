<?php include("header.php"); ?>

<div class="principal">
	<div class="col-2 menu">
		<?php include("menu.php"); ?>
	</div>
	<div class="col-8">
		<h1>Todos os Templates</h1>
		<div class="opcoes-modulo">
			<a href="inserir-templates.php" class="btn btn-inserir">Novo template</a>
		</div>
		<div class="mensagens">
			<?php if(isset($_GET['msg'])) { 
				if($_GET['msg'] == 1) {
					echo '<div class="alert alert-sucesso">Arquivo criada com sucesso</div>';
				}
				else if($_GET['msg'] == 2) {
					echo '<div class="alert alert-erro">Ops... Ocorreu um erro ao salvar o arquivo</div>';
				}
			} ?>
		</div>
		<table class="tabela">
			<tbody>
				<tr>
					<th>Arquivo</th>
					<th>Data de modificação</th>
					<th>Opções</th>
				</tr>
				<?php 
				$arquivos = lerDiretorio( '../assets/templates/' );
				if(!$arquivos) { ?>
				<tr>
					<td class="nenhum-resultado text-center" colspan="4">Nenhuma arquivo foi encontrada</td>
				</tr>
				<?php } else { 
					foreach ($arquivos as $item) { ?>
				<tr>
					<td><?php echo $item['nome'] ?></td>
					<td class="text-center"><?php echo $item['data'] ?></td>
					<td>
						<a href="editar-templates.php?arquivo=<?php echo $item['nome'] ?>" class="btn btn-editar">Editar</a>
						<a href="excluir-templates.php?arquivo=<?php echo $item['nome'] ?>" class="btn btn-excluir" onclick="return excluir();">Excluir</a>
					</td>
				</tr>
				<?php }} ?>
			</tbody>
		</table>
	</div>
</div>

<?php include("footer.php"); ?>