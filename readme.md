# CMS File Web 1.0

Este projeto foi desenvolvido para fins de avaliação da matéria de Linguagem de Programação Web I do Curso de Ciência da Computação da Universidade de Guarulhos UnG

## Instruções

O sistema possui um painel administrativo que é acessível pelo endereço http://www.seusite.com.br/admin

Para acessar o painel, por default o usuário existente é:

- Usuário: admin
- Senha: admin

Disponível em: http://ung.rodrigop.com.br

## Desenvolvedor

Rodrigo Passos (http://www.rodrigop.com.br)

## Ajude o desenvolvedor

Pague-me uma cerveja! ;)