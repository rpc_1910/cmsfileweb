<?php
require_once('admin/funcoes/funcoes.php');

$config = lerArquivo('assets/config/config.txt');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php echo $config['nome']; ?></title>
	<style>
		body {
			text-align: center;
			padding: 50px;
		}
	</style>
</head>
<body>
	<?php echo nl2br($config['msg_manutencao']) ?>
</body>
</html>