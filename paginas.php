<?php
require_once('admin/funcoes/funcoes.php');

$config = lerArquivo('assets/config/config.txt');

if($config['status'] == "2") {
	include('manutencao.php');
	exit;
}

if(!is_file("assets/templates/" . $config['template_interna'])) {
	die("O template selecionado n&atilde;o existe!!");
}


$pagina = lerArquivo('assets/paginas/' . $_GET['arquivo']);

// Captura informações do template
$template = file_get_contents("assets/templates/" . $config['template_interna']);

// Troca informações
if($pagina) {
	$template = preg_replace("/\[TITULO_PAGINA\]/", $pagina['nome'] . ' - ' . $config['nome'], $template);
	$template = preg_replace("/\[DESCRIPTION\]/", $pagina['descricao'], $template);
	$template = preg_replace("/\[KEYWORDS\]/", $pagina['keywords'], $template);
}
else {
	$template = preg_replace("/\[TITULO_PAGINA\]/", $config['nome'], $template);
	$template = preg_replace("/\[DESCRIPTION\]/", $config['descricao'], $template);
	$template = preg_replace("/\[KEYWORDS\]/", $config['keywords'], $template);
}

$template = preg_replace("/\[MENU_PRINCIPAL\]/", getMenuPrincipal('assets/config/menu.txt'), $template);
$template = preg_replace("/\[LOGO\]/", getLogo( $config ), $template);
$template = preg_replace("/\[CSS\]/", getCss('assets/css/estilos.css'), $template);

// Troca conteúdo interno
$template = preg_replace("/\[CONTEUDO_INTERNO\]/", getConteudo($pagina), $template);



//Captura campos
preg_match_all("/\[CAMPO__(.*)\]/", $template, $campos);

if($campos[1]) {
	foreach ($campos[1] as $campo) {
		$template = preg_replace("/\[CAMPO__$campo\]/", getCampo("assets/campos/$campo.txt"), $template);
	}
}


echo $template;